#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""Openstack util functions for xtesting (pre-task)."""

import time


def create_networks(cloud, networks, shared_network):
    """Create networks & subnets."""
    net_created = {}
    subnet_created = {}
    for network in networks:
        net_created[network['name']] = (
            cloud.network.create_network(
                name=network['name'], shared=shared_network,
                admin_state_up=True, external=False))
        subnet_created[network['name']] = (
            cloud.network.create_subnet(
                network_id=net_created[network['name']].id,
                ip_version='4',
                name=network['subnet_name'],
                cidr=network['cidr'], enable_dhcp=True))


def delete_networks(cloud, networks):
    """Delete networks and subnets."""
    for network in networks:
        network_to_delete = cloud.network.find_network(network['name'])
        for network_to_delete_subnet in network_to_delete.subnet_ids:
            cloud.network.delete_subnet(network_to_delete_subnet,
                                        ignore_missing=False)
        cloud.network.delete_network(network_to_delete,
                                     ignore_missing=False)


def check_stack_is_complete(cloud, stack_name):
    """Check the status of a stack."""
    # we assume that the stack does exist
    stack_status_complete = False
    nb_try = 0
    nb_try_max = 10
    while stack_status_complete is False and nb_try < nb_try_max:
        try:
            stack_status = cloud.search_stacks(
                name_or_id=stack_name)[0]['status']
            if "COMPLETE" in stack_status:
                return True
        except IndexError:
            # Stack not found, wait
            nb_try += 1
        time.sleep(10)
    return stack_status_complete
