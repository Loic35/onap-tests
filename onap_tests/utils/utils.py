#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
"""Utils class."""
from difflib import SequenceMatcher

import glob
import random
import uuid
import string
import os
from os.path import basename
import zipfile
import oyaml as yaml

import onap_tests.utils.exceptions as onap_test_exceptions


# ----------------------------------------------------------
#
#               YAML UTILS
#
# -----------------------------------------------------------
def get_parameter_from_yaml(parameter, config_file):
    """
    Get the value of a given parameter in file.yaml.

    Parameter must be given in string format with dots
    Example: general.openstack.image_name
    :param config_file: yaml file of configuration
    :return: the value of the parameter
    """
    with open(config_file) as my_file:
        file_yaml = yaml.safe_load(my_file)
    my_file.close()
    value = file_yaml

    # Ugly fix as workaround for the .. within the params in the yaml file
    ugly_param = parameter.replace("..", "##")
    for element in ugly_param.split("."):
        value = value.get(element.replace("##", ".."))
        if value is None:
            raise ValueError("Parameter %s not defined" % parameter)

    # if value:
    #    print("Param %s => %s" % (parameter,value))
    return value


def get_config(parameter):
    """
    Get configuration parameter from yaml configuration file.

    :param parameter: the searched parameter
    :return: the value of the parameter
    """
    local_path = os.path.dirname(os.path.abspath(__file__))
    yaml_ = local_path.replace(
        "utils", "onap-conf/onap-testing.yaml")
    return get_parameter_from_yaml(parameter, yaml_)


def get_service_custom_config(*args):
    """
    Get Service related configuration parameters from yaml configuration file.

      * args[0]: service_type (ims, vfw, ansible, mrf, freeradius)
      * args[1]: a parameter, if not specified consider the whole file
    :return: the service custom config
    """
    service_type = args[0]
    try:
        parameter = service_type + "." + args[1]
    except IndexError:
        parameter = service_type

    local_path = os.path.dirname(os.path.abspath(__file__))
    yaml_ = local_path.replace("utils",
                               "templates/vnf-services/" +
                               service_type + "-service.yaml")
    return get_parameter_from_yaml(parameter, yaml_)


def get_subscription_type(vnf):
    """
    Get the VNF subscription type (default: vnf).

    :param vnf: the VNF
    :return: the subscription type associated with the VNF
    """
    mytype = ""
    try:
        mytype = get_service_custom_config(vnf, "subscription_type")
    except FileNotFoundError:
        mytype = vnf
    return mytype


def get_service_list():
    """
    Get available service list ready for onboarding.

    :return: the list of the available services (already onboarded on SDC)
    """
    local_path = os.path.dirname(os.path.abspath(__file__))
    yaml_ = local_path.replace("utils", "templates/vnf-services")
    services = []
    for file in os.listdir(yaml_):
        with open(yaml_ + "/" + file) as my_file:
            file_yaml = yaml.safe_load(my_file)
        my_file.close()
        data = file_yaml
        for key in data:
            services.append(key)
    return services


def get_template_param(service_type, parameter):
    """
    Get network service template.

    :param service_typ: the type of service
    :param parameter: the parameter
    :return: the value of the paramter from the config file
    """
    service_name = service_type.capitalize()
    local_path = os.path.dirname(os.path.abspath(__file__))
    template_path = ("templates/tosca_files/" +
                     "service-" + service_name + "-template.yml")
    yaml_ = local_path.replace("utils",
                               template_path)
    return get_parameter_from_yaml(parameter, yaml_)


def heat_env_to_params():
    """Get available heat env param to fill in template/vnf-services."""
    local_path = os.path.dirname(os.path.abspath(__file__))
    yaml_ = local_path.replace("utils", "templates/heat_files")
    data_envs = []
    seq = []
    files = glob.glob(yaml_ + '/**/*.env', recursive=True)
    for file in files:
        with open(file) as my_file:
            file_yaml = yaml.safe_load(my_file)
        my_file.close()
        file_name = yaml_ + '/params.txt'
        dest_file = open(file_name, 'w')
        seq.append("\n")
        seq.append(file)
        seq.append("\n\n")
        seq.append("          vnf_parameters: [")
        seq.append("\n")
        for param_name, param_value in file_yaml['parameters'].items():
            new_line1 = "              {\"vnf-parameter-name\": \"" +\
                str(param_name) + "\","
            new_line2 = "              \"vnf-parameter-value\": \"" +\
                str(param_value) + "\""
            new_line3 = "              },"
            seq.append(new_line1)
            seq.append("\n")
            seq.append(new_line2)
            seq.append("\n")
            seq.append(new_line3)
            seq.append("\n")
        seq.append("          ]")
        seq.append("\n\n\n\n")
        dest_file.writelines(seq)
        dest_file.close()
        data_envs.append(file_yaml)


def get_filtered_template_param(service_type, parameter, myfilter, f_value):
    """
    Filter list according to requirement information.

    :param service_type: the type of the service
    :param parameter: searched parameter
    :param myfilter: filter
    :param f_value:searched value
    :return: the filtered list
    """
    newlist = []
    node_list = get_template_param(service_type, parameter)
    for node in node_list:
        value = get_template_param(
            service_type,
            "topology_template.node_templates." + node + "." + myfilter)
        if f_value in value:
            print(node, value)
            if node not in newlist:
                newlist.append(node)
    return newlist


def order_vnf_list(vnf_list, servicename):
    """
    Sort and order VNF list according to requirement information.

    :param vnf_list: the list of VNFs (e.g {vFW, vPKG})
    :param servicename: the name of the service (e.g. vfw)
    """
    newlist = []
    idx = 0
    for vnf in vnf_list:
        if vnf not in newlist:
            newlist.append(vnf)
            idx += 1
        vnfcontent = get_template_param(
            servicename,
            "topology_template.node_templates." + vnf)
        if 'requirements' in vnfcontent:
            for relation in vnfcontent['requirements'][0].values():
                vnfname = relation['node']
                if vnfname not in newlist:
                    newlist.insert(idx - 1, vnfname)
    return newlist


# ----------------------------------------------------------
#
#               Misc
#
# -----------------------------------------------------------
def random_string_generator(size=6,
                            chars=string.ascii_uppercase + string.digits):
    """
    Get a random String for VNF.

    6 alphanumerical char for CI (to get single instances)
    :return: a random sequence of 6 characters
    """
    return ''.join(random.choice(chars) for _ in range(size))

def get_uuid():
    """
    Get a random UUID String for every purpose.

    :return: a string representing a UUID
    """
    return str(uuid.uuid4())

def get_vf_module_index(vnf_list, target):
    """
    Get VF module index.

    :param vnf_list: the list of VNFs
    :param target: VNF considered for the processing
    :return: the VNF list index of the researched VNF
    """
    # until we understand how to match vnf & vf from the service template
    best_index = 0
    best_index_proba = 0
    for i, elt in enumerate(vnf_list):
        current_proba = SequenceMatcher(None,
                                        target.lower(),
                                        elt.lower()).ratio()
        if current_proba > best_index_proba:
            best_index = i
            best_index_proba = current_proba
    return best_index


def get_vnf_parameters(vnf_type, vnf_name):
    """
    Get the VNF parameters for the SDNC preload.

    :param vnf_type: the type of VNF
    :param vnf_name: the name of the VNF
    :return: the VNF paramters
    """
    # Retrieve the parameters for all the vnf of the vnf_type
    vnf_param = None
    try:
        all_params = get_service_custom_config(vnf_type, "vnfs")
        # Retrieve only the param corresponding to the vnf_name
        for vnf in all_params:
            if vnf['vnf_name'].lower() in vnf_name.lower():
                vnf_param = vnf['vnf_parameters']
    except FileNotFoundError:
        vnf_param = []
    return vnf_param


def get_net_parameters(vnf_type, net_name):
    """
    Get the network parameters for the SDNC preload.

    :param vnf_type: the type of VNF
    :param vnf_name: the name of the VNF
    :return: the network parameters
    """
    # Retrieve the parameters for all the net of the vnf_type
    net_param = None
    try:
        all_params = get_service_custom_config(vnf_type, "networks")
        # Retrieve only the param corresponding to the net_name
        for net in all_params:
            if net['network_name'].lower() in net_name.lower():
                net_param = net['subnets']
    except FileNotFoundError:
        net_param = []
    return net_param


def get_vnf_instance_names(vnf_type):
    """Retrieve VNF instance names from service template."""
    vnf_instance_names = None
    try:
        all_params = get_template_param(
            vnf_type,
            "topology_template.node_templates")
        vnf_instance_names = list(all_params.keys())
    except FileNotFoundError:
        vnf_instance_names = []
    return vnf_instance_names


def create_zip_dir(heat_file_path):
    """
    Create zip directory.

    :param heat_file_path: the path of the VNF heat file
    """
    # create a zip file
    # the heat_file_pat is a absolute path
    # the last part is the expected file name
    # e.g.
    # /a/b/c/d/foo.zip
    # foo.zip is the target zip file to be built
    # it shall contain all the files available in d
    # ziph is zipfile handleimport sysimport sys
    try:
        heat_dir = heat_file_path.rsplit('/', 1)[-2]
        zip_file = zipfile.ZipFile(heat_file_path, 'w', zipfile.ZIP_DEFLATED)
    except IndexError:
        raise onap_test_exceptions.SdcHeatZipException
    except FileNotFoundError:
        raise onap_test_exceptions.SdcHeatZipException

    # pylint: disable=unused-variable
    for root, dirs, files in os.walk(heat_dir):
        for my_file in files:
            my_path = os.path.join(root, my_file)
            if not my_path.endswith('.zip'):
                zip_file.write(my_path, basename(my_path))
    zip_file.close()
