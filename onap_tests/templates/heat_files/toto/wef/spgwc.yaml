#
# This Heat template has been worked out by Nathalie Labidurie to fit the ONAP requirements available here: https://onap.readthedocs.io/en/latest/submodules/vnfrqts/requirements.git/docs/Chapter5/Heat/ONAP%20Heat%20Orchestration%20Template%20Format.html#heat-template-version
# based on the multiple examples available here : https://gitlab.com/Orange-OpenSource/lfn/onap/onap-tests/tree/master/onap_tests/templates/heat_files
# and based on the feedback provided by the little VVP portal available here: http://littlevvp.opnfv.fr/vvp/
#
# See comments on README file

heat_template_version: 2019-03-06 

description: >
   WEF-C SPGWC node

############################################
# SPGWC PARAMETERS
###########################################
parameters:
# vnf_name, vnf_id and vf_module_id are ONAP mandatory parameters
 vnf_name: 
  type: string
  label: VM name
  description: The VM name

 vnf_id: 
  type: string
  label: VNF ID
  description: The VNF ID is provided by ONAP

 vf_module_id: 
  type: string
  label: VF module ID
  description: The VF Module ID is provided by ONAP

 spgwc_name_0:
  type: string
  label: SPGWC Name
  description: SPGWC Name

 spgwc_image_name:
  type: string
  label: SPGWC Image
  description: SPGWC image

# A flavor needs to be declared for each VNF
 spgwc_flavor_name: 
  type: string
  label: UGW Flavor
  description: UGW Flavor

 spgwc_management_ip_0:
  type: string
  label: SPGWC fixed IP address towards the management network
  description: ixed IP address that is assigned to the SPGWC on the management network

 spgwc_core_control_ip_0:
  type: string
  label: SPGWC fixed IP address towards the core control network
  description: fixed IP address that is assigned to the SPGWC on the core control network

############################################
# WEF NETWORK & OTHER PARAMETERS
###########################################
 key_name:
  type: string
  label: WEF-C key 
  description: Key common to all Wef-C VNFs

 availability_zone_0:
  type: string
  label: availability zones for the resource
  description: availability zones for the resource

 management_net_name:
  type: string
  label: Management network name or ID
  description: The management network name

 management_subnet:
  type: string
  label: Management subnetwork name or ID
  description: The management subnetwork

 management_net_cidr:
  type: string
  label: management network CIDR
  description: The CIDR of the management network
 
 core_control_net_name:
  type: string
  label: Core control network name or ID
  description: The core control network name

 core_control_subnet:
  type: string
  label: Core control subnetwork name or ID
  description: The Core control subnetwork

 core_control_net_cidr:
  type: string
  label: Core control network CIDR
  description: The CIDR of the core control network

 shared_security_group:
  type: string
  label: Security group common to all WEF-C VNFs
  description: Security group common to all WEF-C VNFs

 vnf_complete_notify:
  type: string
  label: SoftwareConfig common to all WEF-C VNFs
  description: SoftwareConfig common to all WEF-C VNFs


############################################
# SPGWC RESSOURCES
###########################################
resources:

 spgwc_init:
  type: OS::Heat::MultipartMime
  properties:
   parts:
    - config: {get_param: vnf_complete_notify }

# Specify the SPGWC fixed IP address on the management network
 spgwc_0_management_port_0:
  type: OS::Neutron::Port
  properties:
   network: { get_param: management_net_name }
   fixed_ips: [{"subnet": { get_param: management_subnet }, "ip_address": { get_param: spgwc_management_ip_0 }}]
   security_groups: [{ get_param: shared_security_group }]

# Specify the SPGWC fixed IP address on the core control network
 spgwc_0_core_control_port_0:
  type: OS::Neutron::Port
  properties:
   network: { get_param: core_control_net_name }
   fixed_ips: [{"subnet": { get_param: core_control_subnet }, "ip_address": { get_param: spgwc_core_control_ip_0 }}]
   security_groups: [{ get_param: shared_security_group }]

# Instantiate the SPGWC VNF
 spgwc_0:
  type: OS::Nova::Server
  properties:
   name: { get_param: spgwc_name_0 }
   metadata:
    vnf_id: { get_param: vnf_id } # ONAP mandatory parameter
    vf_module_id: { get_param: vf_module_id } # ONAP mandatory parameter
    vnf_name: { get_param: vnf_name } # ONAP mandatory parameter
    groups: wefc-spgwc
   image: { get_param: spgwc_image_name }
   flavor: { get_param: spgwc_flavor_name }
   availability_zone: { get_param: availability_zone_0 }
   key_name: { get_param: key_name }
   admin_user: ubuntu
   networks:
    - port: { get_resource: spgwc_0_management_port_0 }
    - port: { get_resource: spgwc_0_core_control_port_0 }
   user_data_format: RAW
   user_data:
    get_resource: spgwc_init
    str_replace:
     params:
      __spgwc_management_ip_0__ : { get_param: spgwc_management_ip_0 }
      __management_net_cidr__ : { get_param: management_net_cidr }
      __spgwc_core_control_ip_0__ : { get_param: spgwc_core_control_ip_0 }
      __core_control_net_cidr__ : { get_param: core_control_net_cidr }