#!/usr/bin/env python
# coding: utf-8
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
"""VSP class used for onboarding."""
import logging
import json
from copy import deepcopy

import onap_tests.utils.sender
import onap_tests.utils.exceptions as onap_exceptions
import onap_tests.utils.utils as onap_utils

PROXY = onap_utils.get_config("general.proxy")
SDC_URL = onap_utils.get_config("onap.sdc.url")
SDC_URL2 = onap_utils.get_config("onap.sdc.url2")
SDC_HEADERS = onap_utils.get_config("onap.sdc.headers")
SDC_HEADERS_DESIGNER = SDC_HEADERS.copy()
SDC_HEADERS_DESIGNER["USER_ID"] = "cs0008"
SDC_UPLOAD_HEADERS = onap_utils.get_config("onap.sdc.upload_headers")


class VSP():
    """ONAP VSP Object used for SDC operations."""

    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Init VSP class."""
        # pylint: disable=invalid-name
        self.id = ""
        if "vsp_name" in kwargs:
            self.name = kwargs['vsp_name']
        else:
            self.name = "ONAP-test-VSP"
        self.version = ""
        self.status = ""
        self.csar_uuid = ""
        self.vendor_name = ""
        self.__sender = onap_tests.utils.sender.Sender()

    def __send_message(self, method, action, url, header, **kwargs):
        trans_id = onap_utils.get_uuid()
        header.update(
            {"X-TransactionId": trans_id})
        return self.__sender.send_message('SDC', method, action,
                                          url, header, **kwargs)

    def __send_message_json(self, method, action, url, header, **kwargs):
        trans_id = onap_utils.get_uuid()
        header.update(
            {"X-TransactionId": trans_id})
        return self.__sender.send_message_json('SDC', method, action,
                                               url, header, **kwargs)

    def update_vsp(self, **kwargs):
        """Update VSP vendor values."""
        if "id" in kwargs:
            self.id = kwargs['id']
        if "name" in kwargs:
            self.name = kwargs['name']
        if "version" in kwargs:
            self.version = kwargs['version']
        if "status" in kwargs:
            self.status = kwargs['status']
        if "csar_uuid" in kwargs:
            self.csar_uuid = kwargs['csar_uuid']
        if "vendor_name" in kwargs:
            self.vendor_name = kwargs['vendor_name']

    def get_sdc_vsp_payload(self, **kwargs):
        """Build SDC VSP payload."""
        try:
            payload = {}
            if kwargs['action'] == "Create":
                payload = onap_utils.get_config(
                    "vsp_payload.vsp_create_data")
                if "vsp_name" in kwargs:
                    payload['name'] = kwargs['vsp_name']
                else:
                    payload['name'] = "ONAP-test-VSP"
            if kwargs['action'] == "Commit":
                payload = onap_utils.get_config(
                    "vsp_payload.vsp_commit_data")
            if kwargs['action'] == "Validate":
                payload = onap_utils.get_config(
                    "vsp_payload.vsp_validate_data")
            if kwargs['action'] == "Create_Package":
                payload = onap_utils.get_config(
                    "vsp_payload.vsp_create_package_data")
            if kwargs['action'] == "Submit":
                payload = onap_utils.get_config(
                    "vsp_payload.vsp_submit_data")
            if kwargs['action'] == "NewVersion":
                payload = onap_utils.get_config(
                    "vsp_payload.new_vsp_version_data")
        except KeyError:
            self.__logger.error("No payload set")
        return payload

    def get_vsp_list(self):
        """Get vsp list."""
        url = SDC_URL2 + onap_utils.get_config(
            "onap.sdc.list_vsp_url")
        headers = SDC_HEADERS_DESIGNER
        return self.__send_message_json(
            'GET', 'get vsps', url, headers,
            exception=onap_exceptions.VspNotFoundException)

    def get_last_version(self, vsp_versions):
        """Get the last VSP versions."""
        highest_version = {}
        list_version = []
        for elem in vsp_versions['results']:
            list_version.append(int(elem['name'][:-2]))
        max_version = max(list_version)
        for elem in vsp_versions['results']:
            if int(elem['name'][:-2]) == int(max_version):
                highest_version = deepcopy(elem)
        self.update_vsp(status=highest_version['status'])
        self.update_vsp(version=highest_version['id'])
        self.__logger.debug("VERSION : %s", self.version)

    def check_vsp_exists(self):
        """Check if provided vsp exists in vsp list."""
        vsp_list = self.get_vsp_list()
        vsp_found = False
        for result in vsp_list['results']:
            if result['name'] == self.name:
                vsp_found = True
                self.__logger.debug(
                    "VSP data : %s",
                    result)
                self.update_vsp(id=result['id'])
                old_str = onap_utils.get_config(
                    "onap.sdc.get_vsp_status_url")
                new_str = old_str.replace("PPvsp_idPP", self.id)
                url = SDC_URL2 + new_str
                headers = SDC_HEADERS_DESIGNER
                response = self.__send_message_json('GET', 'get vsp',
                                                    url, headers)
                if response:
                    self.get_last_version(response)
                    self.update_vsp(vendor_name=result['vendorName'])

        if not vsp_found:
            self.update_vsp(id="")
            self.update_vsp(version="")
            self.update_vsp(status="")
            self.update_vsp(vendor_name="")
            self.update_vsp(csar_uuid="")
        return vsp_found

    def get_vsp_info(self):
        """Get VSP details."""
        vsp_list = self.get_vsp_list()
        for result in vsp_list['results']:
            if result['name'] == self.name:
                self.update_vsp(version=result["version"]["id"])
                self.update_vsp(status=result["status"])
                break

    def create_vsp(self, vendor):
        """Create a VSP in SDC (only if it does not exist)."""
        # we check if vsp exists or not, if not we create it
        create_vsp = False
        if not self.check_vsp_exists():
            url = SDC_URL2 + onap_utils.get_config(
                "onap.sdc.create_vsp_url")
            data = self.get_sdc_vsp_payload(action="Create",
                                            vsp_name=self.name)
            data["vendorName"] = vendor.name
            data["vendorId"] = vendor.id
            data["name"] = self.name
            data = json.dumps(data)
            headers = SDC_HEADERS_DESIGNER
            response = self.__send_message_json('POST', 'Create vsp',
                                                url, headers, data=data)
            if response:
                create_vsp = True
                self.update_vsp(
                    status=response['version']['status'],
                    id=response['itemId'],
                    version=response['version']['id'],
                    vendor_name=vendor.name)
        else:
            self.__logger.info("VSP already exists")
        return create_vsp

    def new_vsp_version(self):
        """Update new VSP version in SDC."""
        new_vsp_version = False
        old_str = onap_utils.get_config(
            "onap.sdc.new_vsp_version_url")
        new_str = old_str.replace("PPvsp_idPP", self.id)
        old_str = new_str
        new_str = old_str.replace("PPvsp_versionPP", self.version)
        url = SDC_URL2 + new_str
        data = self.get_sdc_vsp_payload(action="NewVersion")
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        response = self.__send_message_json('POST', 'New vsp version',
                                            url, headers, data=data)
        if response:
            new_vsp_version = True
            self.update_vsp(
                status=response['status'],
                version=response['id'])
            self.__logger.debug(
                "[SDC][New vsp version] version id : %s", self.version)
            self.__logger.debug(
                "[SDC][New vsp version] status : %s", self.status)
        return new_vsp_version

    def upload_vsp(self, files):
        """Upload zip for VSP."""
        old_str = onap_utils.get_config("onap.sdc.upload_vsp_url")
        new_str = old_str.replace("PPvsp_idPP", self.id)
        old_str = new_str
        new_str = old_str.replace("PPvsp_versionPP", self.version)
        url = SDC_URL2 + new_str
        headers = SDC_UPLOAD_HEADERS
        return bool(self.__send_message_json('POST', 'upload ZIP VSP',
                                             url, headers, files=files))

    def __action_on_vsp(self, config_path, action):
        """Trigger an action on VSP (Validate, Commit, Submit, Create CSAR)."""
        old_str = onap_utils.get_config(config_path)
        new_str = old_str.replace("PPvsp_idPP", self.id)
        old_str = new_str
        new_str = old_str.replace("PPvsp_versionPP", self.version)
        url = SDC_URL2 + new_str
        data = self.get_sdc_vsp_payload(action=action)
        data = json.dumps(data)
        headers = SDC_HEADERS_DESIGNER
        return self.__send_message_json(
            'PUT', "{} vsp".format(action), url, headers, data=data,
            exception=onap_exceptions.VspOnboardException)

    def validate_vsp(self):
        """Validate VSP."""
        return bool(self.__action_on_vsp("onap.sdc.validate_vsp_url",
                                         "Validate"))

    def commit_vsp(self):
        """Commit VSP."""
        return bool(self.__action_on_vsp("onap.sdc.commit_vsp_url", "Commit"))

    def submit_vsp(self):
        """Submit VSP."""
        return bool(self.__action_on_vsp("onap.sdc.submit_vsp_url", "Submit"))

    def create_package_vsp(self):
        """Create CSAR Package."""
        response = self.__action_on_vsp("onap.sdc.create_package_vsp_url",
                                        "Create_Package")
        if response:
            self.update_vsp(csar_uuid=response["packageId"])
            return True
        return False
