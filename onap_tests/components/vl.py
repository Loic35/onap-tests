#!/usr/bin/env python
# coding: utf-8
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=missing-docstring
#  pylint: disable=too-many-instance-attributes
"""Virtual Link class used for onboarding."""
import logging

import onap_tests.utils.sender
import onap_tests.utils.utils as onap_utils

PROXY = onap_utils.get_config("general.proxy")
SDC_URL = onap_utils.get_config("onap.sdc.url")
SDC_URL2 = onap_utils.get_config("onap.sdc.url2")
SDC_HEADERS = onap_utils.get_config("onap.sdc.headers")
SDC_HEADERS_DESIGNER = SDC_HEADERS.copy()
SDC_HEADERS_DESIGNER["USER_ID"] = "cs0008"
SDC_HEADERS_TESTER = SDC_HEADERS.copy()
SDC_HEADERS_TESTER["USER_ID"] = "jm0007"
SDC_UPLOAD_HEADERS = onap_utils.get_config("onap.sdc.upload_headers")


class VL():
    """ONAP VL Object used for SDC operations."""

    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Init VL class."""
        # pylint: disable=invalid-name
        self.id = ""
        self.unique_id = ""
        if "vl_name" in kwargs:
            self.name = kwargs['vl_name']
        else:
            self.name = "ONAP-test-VL"
        self.vl_name = ""
        self.version = ""
        self.status = ""
        self.posx = 220
        self.posy = 220
        self.__sender = onap_tests.utils.sender.Sender()

    def __send_message(self, method, action, url, header, **kwargs):
        trans_id = onap_utils.get_uuid()
        header.update(
            {"X-TransactionId": trans_id})
        return self.__sender.send_message('SDC', method, action,
                                          url, header, **kwargs)

    def __send_message_json(self, method, action, url, header, **kwargs):
        trans_id = onap_utils.get_uuid()
        header.update(
            {"X-TransactionId": trans_id})
        return self.__sender.send_message_json('SDC', method, action,
                                               url, header, **kwargs)

    def update_vl(self, **kwargs):
        """Update VL values."""
        if "id" in kwargs:
            self.id = kwargs['id']
        if "unique_id" in kwargs:
            self.unique_id = kwargs['unique_id']
        if "name" in kwargs:
            self.name = kwargs['name']
        if "version" in kwargs:
            self.version = kwargs['version']
        if "status" in kwargs:
            self.status = kwargs['status']

    def get_vl_list(self):
        """Get vl list."""
        url = SDC_URL + onap_utils.get_config(
            "onap.sdc.list_vl_url")
        headers = SDC_HEADERS_DESIGNER
        return self.__send_message_json('GET', 'get vls',
                                        url, headers)

    def check_vl_exists(self):
        """Check if provided vl exists in vl list."""
        vl_list = self.get_vl_list()
        vl_found = False
        for result in vl_list:
            if result['name'] == self.vl_name:
                vl_found = True
                self.__logger.info("VL %s found in VL list", self.vl_name)
                self.update_vl(id=result["uuid"])
                self.update_vl(version=result["version"])
                self.update_vl(status=result["lifecycleState"])
        if not vl_found:
            self.__logger.info("VL is not in vl list: %s", self.vl_name)
            self.update_vl(id="")
            self.update_vl(unique_id="")
            self.update_vl(version="")
            self.update_vl(status="")

        if vl_found:
            # Get detailed content of created VL, especially unique_id
            urlpart = onap_utils.get_config(
                "onap.sdc.get_catalog_resource_list_url")
            url = SDC_URL2 + urlpart
            headers = SDC_HEADERS_DESIGNER
            response = self.__send_message_json('GET', 'get vl',
                                                url, headers)
            if response:
                for resource in response["resources"]:
                    if resource["name"] == self.vl_name:
                        self.__logger.info(
                            "[SDC][get vl] VL resource %s found",
                            resource["name"])
                        self.__logger.debug(
                            "[SDC][get vl] VL resource %s uniqueId",
                            resource["uniqueId"])
                        self.update_vl(unique_id=resource["uniqueId"])
            self.__logger.info("[SDC][get vl] VL found parameter %s", vl_found)

        return vl_found

    def get_vl_info(self, vlink):
        """Get VL details."""
        vl_list = self.get_vl_list()
        for result in vl_list:
            if result['name'] == vlink['name']:
                vlink = result
                break
        return vlink
