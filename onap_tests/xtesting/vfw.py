#!/usr/bin/env python
"""vFw test case."""
import logging
import os
import time

import openstack
from openstack.config import loader
from xtesting.core import testcase

import onap_tests.scenario.e2e as e2e
import onap_tests.utils.openstack_utils as openstack_utils

TEST_CLOUD = os.getenv('OS_TEST_CLOUD', 'openlab-vnfs-ci')
loader.OpenStackConfig()

NETWORK_1 = {'name': 'oam_net-ci',
             'subnet_name': 'oam_subnet-ci',
             'cidr': '10.0.111.0/16'}

NETWORK_2 = {'name': 'zdfw1fwl01_protected-ci',
             'subnet_name': 'zdfw1fwl01_protected_sub-ci',
             'cidr': '193.168.20.0/24'}

NETWORK_3 = {'name': 'zdfw1fwl01_unprotected-ci',
             'subnet_name': 'zdfw1fwl01_unprotected_sub-ci',
             'cidr': '193.168.10.0/24'}
SHARED_NETWORK = False
NETWORKS = [NETWORK_1, NETWORK_2, NETWORK_3]


class Vfw(testcase.TestCase):
    """Onboard then instantiate a simple VM though ONAP."""

    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Virtual Firewall."""
        if "case_name" not in kwargs:
            kwargs["case_name"] = 'vfw'
        super(Vfw, self).__init__(**kwargs)
        self.cloud = openstack.connect(cloud=TEST_CLOUD)
        openstack_utils.create_networks(
            self.cloud, NETWORKS, SHARED_NETWORK)
        self.__logger.debug("vFW init started")
        self.test = e2e.E2E(service_name='vfw')
        self.start_time = None
        self.stop_time = None
        self.result = 0

    def run(self):
        """Run onap_tests with vFW VM."""
        self.start_time = time.time()
        self.__logger.debug("start time")
        try:
            self.test.execute()
            self.__logger.info("VNF vfw successfully created")
            self.test.clean()
            # Clean is part of the test
            self.__logger.info("VNF cleaned")
            self.result = 100
            self.stop_time = time.time()
            return testcase.TestCase.EX_OK
        except Exception:  # pylint: disable=broad-except
            self.result = 0
            self.stop_time = time.time()
            self.__logger.error("vFW test failed.")
            return testcase.TestCase.EX_TESTCASE_FAILED

    def clean(self):
        """Clean VNF resources."""
        openstack_utils.delete_networks(self.cloud, NETWORKS)
