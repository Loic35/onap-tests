#!/usr/bin/env python
"""Clearwater test case."""
import logging
import time

import onap_tests.scenario.e2e as e2e
from xtesting.core import testcase


class ClearwaterIms(testcase.TestCase):
    """Onboard then instantiate clearwater IMS though ONAP."""

    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Init Clearwater IMS VNF."""
        if "case_name" not in kwargs:
            kwargs["case_name"] = 'basic_vm'
        super(ClearwaterIms, self).__init__(**kwargs)
        self.__logger.debug("ClearwaterIms init started")
        self.test = e2e.E2E(service_name='ims')
        self.start_time = None
        self.stop_time = None
        self.result = 0

    def run(self):
        """Run clearwater vIMS."""
        self.start_time = time.time()
        self.__logger.debug("start time")
        try:
            self.test.execute()
            self.__logger.info("VNF clearwaterIms successfully created")
            self.test.clean()
            self.__logger.info("VNF clearwaterIms successfully cleaned")
            self.result = 100
            self.stop_time = time.time()
            return testcase.TestCase.EX_OK
        except Exception:  # pylint: disable=broad-except
            self.result = 0
            self.stop_time = time.time()
            self.__logger.error("Clearwater IMS test failed.")
            return testcase.TestCase.EX_TESTCASE_FAILED

    def clean(self):
        """Clean VNF."""
        pass
