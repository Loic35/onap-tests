#!/usr/bin/env python
"""Basic VM test case."""
import logging
import time

import onap_tests.scenario.e2e as e2e
from xtesting.core import testcase


class BasicVm(testcase.TestCase):
    """Onboard then instantiate a simple VM though ONAP."""

    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Init BasicVM."""
        if "case_name" not in kwargs:
            kwargs["case_name"] = 'basic_vm'
        super(BasicVm, self).__init__(**kwargs)
        self.__logger.debug("BasicVm init started")
        self.test = e2e.E2E(service_name='ubuntu16')
        self.start_time = None
        self.stop_time = None
        self.result = 0

    def run(self):
        """Run onap_tests with ubuntu16 VM."""
        self.start_time = time.time()
        self.__logger.debug("start time")
        self.test.execute()
        self.__logger.info("VNF basic_vm successfully created")
        self.test.clean()
        # Clean is part of the test
        self.__logger.info("VNF cleaned")
        self.result = 100
        self.stop_time = time.time()
        return testcase.TestCase.EX_OK

    def clean(self):
        """Clean VNF."""
        pass
