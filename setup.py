import setuptools
setuptools.setup(
    setup_requires=['pbr', 'setuptools', 'pytest-runner'],
    pbr=True)
